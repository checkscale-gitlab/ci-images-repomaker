FROM registry.gitlab.com/fdroid/ci-images-server:latest
MAINTAINER team@f-droid.org

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash - \
	&& apt-get update && apt-get upgrade -y \
	&& apt-get install -y --no-install-recommends \
		python3-cryptography \
		python3-psycopg2 \
		python3-rcssmin \
		python3-rjsmin \
		python3-wheel \
		build-essential \
		libmagic1 \
		nodejs \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& ln -s /usr/bin/pylint3 /usr/local/bin/pylint \
	&& wget https://gitlab.com/fdroid/repomaker/raw/master/requirements.txt \
	&& pip3 install -r requirements.txt \
	&& rm requirements.txt \
	&& wget https://gitlab.com/fdroid/repomaker/raw/master/requirements-dev.txt \
	&& pip3 install -r requirements-dev.txt \
	&& rm requirements-dev.txt

COPY test /

